(function () {
  'use strict';

  describe('1. Filters Testing',function(){

    describe('1.1 Testing filter "trust"  --->', function() {
      var trustFilter;

      beforeEach(module('WeatherApp'));

      // Important to do before each for generating trusted html
      beforeEach(module(function ($sceProvider) {
        $sceProvider.enabled(false);
      }));

      beforeEach(inject(function(_trustFilter_) {
        trustFilter = _trustFilter_;
      }));

      it('should be able to render trusted html', inject(function($sce){
        expect(trustFilter('<span>hello</span>')).toBe('<span>hello</span>');
      }));

    });


    describe('1.2 Testing filter "UCF"  --->', function() {
      var ucfFilter;

      beforeEach(module('WeatherApp'));

      beforeEach(inject(function(_ucfFilter_) {
        ucfFilter = _ucfFilter_;
      }));

      it('should be able to Sentence case an entire input', function() {
        expect(ucfFilter('hello')).toBe('Hello');
        expect(ucfFilter('hello world')).toBe('Hello world');
      });

    });

  })

}());
