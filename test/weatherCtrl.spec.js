(function () {
	'use strict';

	describe('1. Weather Controller Tests', function () {
    // Initialize required variables
    var ctrl, scope, api;

    beforeEach(module('WeatherApp'));

    beforeEach(inject(function($controller, $rootScope, weatherService){
      scope = $rootScope.$new();
      api = weatherService;

      // Mock Api service used!
      weatherService.getWeather = function(){
        var obj = {
          "city":{
            "name":"Kathmandu",
            "country":"Nepal"
          }
        }
        return obj;
      }

      ctrl = $controller('weatherCtrl', {
				$scope: scope,
				api: api
			});
    }));

    describe('1.1 Controller Variables Defination', function(){
      it('conrtoller should be available', function () {
        expect(ctrl).toBeTruthy()
      });

      it('should not search query on start', function () {
        expect(scope.search_query).toBe("");
      });

      it('should not display the Detailed data', function () {
        expect(scope.data_visible).toBeFalsy();
      });

      it('should have a default temperature to be Degree Centigrade (0C)', function () {
        expect(scope.deg).toBe('c');
      });

      it('should not have any weather data on app start', function () {
        expect(scope.weather_data).toEqual({});
      });
    });

    describe('1.2 Controller Functions Working Tests', function(){

      it('should return the corect payload object for Longitude and latitude', function () {
        var param = { coords:{} };
        expect(scope.payloadGenerator(param,"loglat")).toEqual({ "key":"loglat", "value": param.coords})
      });

      it('should return the corect payload object for Zip or Postal Codes', function () {
        expect(scope.payloadGenerator("44600","zip")).toEqual({ "key":"zip", "value": "44600"})
      });

      it('should return the corect payload object for City', function () {
        expect(scope.payloadGenerator('Kathmandu',"city")).toEqual({ "key":"city", "value": "Kathmandu"})
      });

      it('should return the corect Centegrade time from Kelvin', function () {
        expect(scope.convertTemperature(285.21)).toBe(12)
      });
    })


    describe('1.3 Controller Spys Tests for functions', function(){

      it('should call geolocation_available() on clearing the search Field', function(){
        spyOn(scope, 'geolocation_available');
        scope.clearForm();
        expect(scope.geolocation_available).toHaveBeenCalled();
      });

      it('should call ready_for_data(Param) on submitting the location search form', function(){
        spyOn(scope, 'ready_for_data');
        scope.searchForecast();
        expect(scope.ready_for_data).toHaveBeenCalled();
      });

    })

	});
}());
