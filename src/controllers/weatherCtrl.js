
angular.module('WeatherApp')
	.controller('weatherCtrl',['$scope','api', function($scope, api) {
		'use strict';

    // $scope variables definations
    $scope.search_query = "";
    $scope.data_visible = false;
    $scope.deg = 'c';
    $scope.payload = {};
    $scope.weather_data = {};

    /**
     * [saveLocation Store location in the localStorage for easy retrival and minimize frequent server requests]
     * @param  values
     * @param  data_type
     * @return  Pass call
     */
    function saveLocation(values, data_type) {
      api.getWeather(values, data_type).then(function(data){
        localStorage._wdc = JSON.stringify({
          timestamp:(new Date()).getTime(),
          data: data
        })
        $scope.ready_for_data(data, "loglat", "detail");
      })
    }

    /**
     * [geolocation_available Check if the locatin is available of not]
     * @return Pass a call
     */
    $scope.geolocation_available = function() {
      if (navigator.geolocation) {
				Materialize.toast("Geolocation Supported!", 1000)
        return navigator.geolocation.getCurrentPosition($scope.locationSuccess, $scope.locationError)
      }
    }

    /**
     * [locationSuccess If location found passes the call and if not displays error]
     * @param  position
     * @return Pass or Error
     */
    $scope.locationSuccess = function(position) {
      try{
        $scope.ready_for_data(position,"loglat","detail")
      }
      catch(e){
        Materialize.toast("We can't find information about your city!", 2000)
        console.log('Exception: ', e)
      }
    }

    /**
     * [locationError Handles the location errors]
     * @param  error
     * @return Void
     */
    $scope.locationError = function(error){
      switch(error.code) {
        case error.TIMEOUT:
        	Materialize.toast("A timeout occured! Please try again!", 2000);
        	break;
        case error.POSITION_UNAVAILABLE:
        	Materialize.toast('We can\'t detect your location. Sorry!', 2000);
        	break;
        case error.PERMISSION_DENIED:
        	Materialize.toast('<img src="images/rings.svg" alt="" style="color:redheight: 28px">&nbsp Please allow geolocation access for this to work.', 2000);
        	break;
        case error.UNKNOWN_ERROR:
        	Materialize.toast('An unknown error occured!', 2000);
        break
      }
    }

    /**
     * [convertTemperature Converts the temperature]
     * @param  kelvin
     * @return Number
     */
    $scope.convertTemperature = function(kelvin){
      // Convert the temperature to either Celsius or Fahrenheit:
      return Math.round($scope.deg == 'c' ? (kelvin - 273.15) : (kelvin*9/5 - 459.67));
    }

    /**
     * [searchForecast Handles the search for the appropriate location weather]
     * @return pass call to another function
     */
    $scope.searchForecast = function() {
      if (isNaN($scope.search_query)) {
        $scope.ready_for_data($scope.search_query,"city","detail");
      }
      else{
        $scope.ready_for_data($scope.search_query,"zip","detail");
      }
    }

    /**
     * [clearForm  Searched for the current colcation on clearing the Search form]
     * @return void
     */
    $scope.clearForm = function(){
      $scope.search_query = "";
      $scope.geolocation_available();
    }

    /**
     * [ready_for_data performs the main operation to manipulate the location data]
     * @param  param
     * @param  flag
     * @param  data_type
     * @return void
     */
    $scope.ready_for_data = function(param, flag, data_type){
			Materialize.toast('<img src="images/rings.svg" alt="" style="color:redheight: 28px">&nbsp Now Searching weather for ' + ($scope.search_query || ' your Location'),3000)
      // Generating the payload based on example
      $scope.payload = $scope.payloadGenerator(param,flag);

      // Calling the Remote services
      var cache = localStorage._wdc && JSON.parse(localStorage._wdc);
      var d = new Date();

      if(cache && cache.timestamp && cache.timestamp > d.getTime() - 30*60){
        console.log(cache)
        // Creating the Weather data Object form available data
        $scope.search_query = cache.data.city.name;
				$scope.weather_data.city = cache.data.city;
				$scope.weather_data.today = cache.data.list[0];
				$scope.weather_data.today.link = '<object type="image/svg+xml" data="images/icons/'+$scope.weather_data.today.weather[0].icon+'.svg" height="130"></object>'
				cache.data.list.shift();
				$scope.weather_data.remaining = cache.data.list;
        $scope.displayData();
      }else{
        saveLocation($scope.payload, data_type);
      }
    }

		/**
		 * [payloadGenerator description]
		 * @param  param_data
		 * @param  flag_type
		 * @return  Object
		 */
		$scope.payloadGenerator = function(param, flag){
			switch (flag) {
        case "zip":
        return { "key":"zip", "value": param};
        break
        case "city":
        return { "key":"city", "value": param};
        break
        case "loglat":
        return { "key":"loglat", "value": param.coords };
        break
      }
		}

    /**
     * [displayData passes the data to views]
     * @param  {[type]} data_type
     * @return false
     */
    $scope.displayData = function() {
      $('#search-bar').attr('style','margin-top:-78px;');
      $('.logo').attr('style','height:65px;');
      return false;
    }

  }])
