angular  = require('angular')
require('angular-route')

angular.module('WeatherApp', ['ngRoute'])
	.config(function ($routeProvider) {
		$routeProvider
			.when('/welcome', {
  			controller: 'weatherCtrl',
  			templateUrl: 'templates/home.html',
        resolve: {
  				api: ['weatherService', function (api) {
  					return api;
  				}]
  			}
  		})
			.otherwise({
				redirectTo: '/welcome'
			});
	});

  // Loading the necessary modules using the browesrify!
  var weatherCtrl = require('weatherCtrl');
  var weatherService = require('weatherService');
  var footer = require('footer');
  var trust = require('trust');
  var ucf = require('ucf');
