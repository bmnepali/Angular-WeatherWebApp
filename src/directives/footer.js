angular.module("WeatherApp").directive("footer", function() {
  return {
    restrict: 'E',
    templateUrl: 'templates/footer.html',
    scope: true,
    transclude : false
  };
});
