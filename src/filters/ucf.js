angular.module('WeatherApp')
  .filter('ucf', function(){
    return function(word){
        return word.substring(0,1).toUpperCase() + word.slice(1);
    }
});
