angular.module('WeatherApp')
  .factory('weatherService', ['$http', '$q', function($http, $q) {
    /**
    * [getWeather gets weather data form the server based on calltype and paramaters]
    * @param  {[Object]} param
    * @param  {[String]} call_type
    * @return {[JSON Object]}
    */
    function getWeather(param, call_type) {
      var flag = (call_type == undefined)? 'detail' : call_type
      var endpoint_root = "http://api.openweathermap.org/data/2.5"
      var api_key = "35176ec56558af7eea98ff15599e371f"
      var endpoint = "/forecast/daily"
      var query_strings = ""

      switch (param.key) {
        case "zip":
        query_strings = "&zip=" + param.value
        break
        case "city":
        query_strings = "&q=" + param.value
        break
        case "loglat":
        query_strings = "&lat=" + param.value.latitude.toFixed(2) + "&lon="+ param.value.longitude.toFixed(2)
        break
      }

      // Gnerating the Api endpoint
      var api_call_path = endpoint_root + endpoint +"?APPID=" + api_key + query_strings
      var deferred = $q.defer()
      $http.get(api_call_path)
      .success(function(data) {
        deferred.resolve(data)
      })
      .error(function(err) {
        Materialize.toast('Problem while fetching Weather Data! form Server',1500)
        deferred.reject(err)
      })
      return deferred.promise
    }

    return { getWeather: getWeather}
  }])
