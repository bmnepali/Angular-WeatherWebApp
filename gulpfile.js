
var gulp = require('gulp'),
    webserver = require('gulp-webserver'),
    del = require('del'),
    concat = require("gulp-concat");
    sass = require('gulp-sass'),
    karma = require('gulp-karma'),
    jshint = require('gulp-jshint'),
    sourcemaps = require('gulp-sourcemaps'),
    spritesmith = require('gulp.spritesmith'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    gutil = require('gulp-util'),
    ngAnnotate = require('browserify-ngannotate');

var CacheBuster = require('gulp-cachebust');
var cachebust = new CacheBuster();

//------------------------------------------------------------------------------
// cleans the build output
//------------------------------------------------------------------------------

gulp.task('clean', function () {
    del(['dist'],{dryRun:true});
});

//------------------------------------------------------------------------------
// Copy all fonts
//------------------------------------------------------------------------------
gulp.task('copy:fonts', function () {
    gulp.src('./assets/fonts/**/*.{ttf,woff,woff2,eof,svg}')
    .pipe(gulp.dest('./dist/fonts/'))
});

//------------------------------------------------------------------------------
// Copy all Templates
//------------------------------------------------------------------------------
gulp.task('copy:templates', function () {
    gulp.src('./src/templates/*.html')
    .pipe(gulp.dest('./dist/templates/'))
});

//------------------------------------------------------------------------------
// Copy all vendors Libraries to one vendors.min.js
//------------------------------------------------------------------------------
gulp.task('copy:libs',['bower'], function () {
    gulp.src([
      './assets/libs/*.js'
    ])
    .pipe(concat('libs.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/libs'))
});


//------------------------------------------------------------------------------
// Copy all images to images
//------------------------------------------------------------------------------
gulp.task('copy:images', function () {
    gulp.src('./assets/images/**/*')
    // Perform here all the image optimizations activities
    .pipe(gulp.dest('./dist/images'))
});

//------------------------------------------------------------------------------
// runs bower to install frontend dependencies
//------------------------------------------------------------------------------
gulp.task('bower', function() {
    var install = require("gulp-install");
    return gulp.src(['./bower.json'])
        .pipe(install());
});

//------------------------------------------------------------------------------
// runs sass, creates css source maps
//------------------------------------------------------------------------------
gulp.task('build-css', function() {
    return gulp.src('./assets/styles/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(cachebust.resources())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/styles'));
});

//------------------------------------------------------------------------------
// runs jshint
//------------------------------------------------------------------------------
gulp.task('jshint', function() {
    gulp.src('/src/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

//------------------------------------------------------------------------------
// runs Karma Unit tests
//------------------------------------------------------------------------------
gulp.task('test', ['build-js'], function() {
    var testFiles = [
        './test/unit/*.js'
    ];

    return gulp.src(testFiles)
        .pipe(karma({
            configFile: 'karma.conf.js',
            action: 'run'
        }))
        .on('error', function(err) {
            console.log('karma tests failed: ' + err);
            this.emit('end')
        });
});

//------------------------------------------------------------------------------
// Build a minified Javascript bundle - the order of the js files
// determined by browserify - only the angularjs files
//------------------------------------------------------------------------------
gulp.task('build-js', function() {
    var b = browserify({
        entries: './src/app.js',
        debug: true,
        paths: ['./src/controllers', './src/services', './src/directives', './src/filters'],
        transform: [ngAnnotate]
    });

    return b.bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(cachebust.resources())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/js/'));
});

//------------------------------------------------------------------------------
// Production build (except sprites), applies cache busting to the main page //s
// and js bundles
//------------------------------------------------------------------------------
gulp.task('build',[
    'copy:images',
    'copy:fonts',
    'build-css',
    'copy:templates',
    'bower',
    'copy:libs',
    'jshint',
    'build-js'
  ], function() {
      return gulp.src('index.html')
        .pipe(cachebust.references())
        .pipe(gulp.dest('dist'));
});

//------------------------------------------------------------------------------
// watches file system and triggers a build when a modification is detected
//------------------------------------------------------------------------------
gulp.task('watch', function() {
    return gulp.watch([
      './index.html',
      './templates/*.html',
      './assets/styles/*.scss',
      './src/**/*.js'],
      ['build']
    );
});

//------------------------------------------------------------------------------
// launches a web server that serves files in the current directory
//------------------------------------------------------------------------------
gulp.task('webserver', ['watch','build'], function() {
    gulp.src('.')
        .pipe(webserver({
            livereload: false,
            directoryListing: true,
            open: "http://localhost:8000/dist/index.html"
        }));
});

//------------------------------------------------------------------------------
// launch a build upon modification and publish it to a running server
//------------------------------------------------------------------------------
gulp.task('dev', ['watch', 'webserver']);

//------------------------------------------------------------------------------
// installs and builds everything, including sprites
//------------------------------------------------------------------------------
gulp.task('default', ['build', 'test']);
