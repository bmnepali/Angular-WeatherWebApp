# Weather Forecast Web Application
=================================

This repository contains a Weather Forecast Web Application, created with love using Angularjs, Matarialize.css, Materialize.js, SCSS.
And Tests are written using Karma, Jasmine, etc.  

## Note:
This App uses the Geolocaton APIs, and nowadays, google chrome and sometimes Safari tends to block the API Calls if the App is not hosted with SSL Certificate.
So, the App is currently best viewed in Mozilla Firefox. If its HTTPS then the App is Best in all Modern Browsers.

## More
The Project is fully developped using the followng gulp and npm packages for the development tomake smooth and automated:

* integration with bower - to ensure all the latest dependencies are present
* Sass (with source maps) - for building more maintainable stylesheets.
* unit testing / integration testing with Karma, Jasmine and PhantomJs
* cache busting - to prevent cache problems in all environments, dev tools opened or closed, etc.
* jshint - the de-facto Javascript code quality checker
* concatenation
* browserify integration - for using the CommonJs *require('module')* synchronous import syntax and avoid maintaining config files with dependencies
* Javascript source maps
* Angular-friendly Javascript Minification
* a development web server with live reload, that re-loads the initial page of the project

# Installation instructions

In Order to run this project, one should have Node.js Installed greater that `nodejs 4.* +`.
And all other activities are handled by gulp.js.

First make sure Gulp is globally installed, by running:
```
    npm install -g gulp
```
After cloning the project, run the following commands:
```
    npm install
    gulp build
    gulp dev
```
# Tests
After you have the project build, you can run following command to run tests:
``` 
gulp test

```

This will start the development server, the Weather Forecast app should be available at the following url:

[http://localhost:8000/dist/index.html](http://localhost:8000/dist/index.html)

MIT
